## Rev 0.1

Looks like this is done. I've mentally triple checked my tx/rx ports wiring. Feeling find about the way this turns out: do two 'ports' on motherboard / most boards. Ports should be software selectable to be either sync or data devices... This way, the hardware is there if we implement hardcore syncing, and if we just source clocks (as seems likely) we just have an extra port. The router will just implement all 5 sercoms ... are there 6? Current router implements 0-1-(x)-3-4-5 ... so SER2 is ... ? RX on SERx-2, TX on SERx-0... by all means SER2 looks open, could do. So 6 ports is 3 motors fully synced on one router, 6 motors clock-source synced. SER7 appears to kick as well... but not available on build. OK. 6.

## Notes

The point is that specialization is rad, so specialized circuits make sense. So this 'motherboard / dboard' thing is kind of confusing. You have a lot of repeating elements, though, so PNP'ing some header modules also makes sense. economies of scale are damning. however, these should be baked on to other circuits: again: the castellated mod here makes sense (or version w/ solder-able-on-headers, like a regular old devboard).

## Connector Notes

SAM14005CT-ND 2x10 Conn is 3.66mm Tall,
With 2.54mm insulation on pins, for 6.2mm ... so use
WURTH M3 Unthreaded 7mm BBH 732-7109-1-ND (beneath)
WURTH M3 Unthreaded 3mm BBH 732-7087-1-ND (above)

## (old) Parts Bits

WURTH M3 Threaded 7mm Tall 732-5274-1-ND
WURTH M3 Unthreaded 10mm Tall 732-7119-1-ND
2x10 0.1" 15.75mm x 2.8mm SAM1066-10-ND‎
2x10 0.1" 15.75mm x 5.84mm S2061E-10-ND‎
2x10 0.1" thru-headers 609-4473-1-ND (amphenol 7mm) or 952-2359-ND (harwin 7.5mm)
0805 10uH Inductor 445-6762-1-ND
RCT XC1617CT-ND  (and find 12.5pF 0603)
16pF 0603 ‎1276-2204-1-ND‎
TXRX LTC2855CDE#TRPBFCT-ND
