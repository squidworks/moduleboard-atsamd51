# Squidworks Moduleboard w/ the ATSAMD51

This is a small, solder-on breakout board for the [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) project. It contains all necessary components to run [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) with great ease, and provides USB and RS485-Over-RJ10 connectivity. It breaks out almost all other pins of the SAMD51 microprocessor onto it's castellated pins (at the edges) and it's LGA pads (on the bottom).

![board](moduleboard-atsamd51/2019-10-30_mw-module.jpg)

![schematic](moduleboard-atsamd51/schematic.png)

![routed](moduleboard-atsamd51/routed.png)

To crib arduino pinouts from Adafruit's M4 interpretation, see:

![adaschem](reference/arduino_compatibles_schem.png)

To program / use see [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) and [SAMD51 notes](https://gitlab.cba.mit.edu/pub/hello-world/atsamd51).

## Using the Board

I've made an eagle footprint for this thing, to include in your projects. That's included here, under `footprints/` - include this in your circuit: there is a version *with* the under-foot pads, and one without.

![fp](footprints/moduleboard-foot_complete.png)
![fp](footprints/moduleboard-foot_castel-only.png)

You can drop this into your circuit, and solder the whole module on. The board has a +3v3 output, with a (rated at) 500mA linear regulator on board, for which it expects a +5v input: this +5v line is tied to the USB vbus.
